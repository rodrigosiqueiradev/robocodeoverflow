package br.eti.rodrigosiqueira;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

public class Overflow extends AdvancedRobot {

    // Frente ou ré
	int moveDirection = 1;

	public void run() {
	    // Radar independente do robo
		setAdjustRadarForRobotTurn(true);
		setBodyColor(new Color(128, 128, 50));
		setGunColor(new Color(50, 50, 20));
		setRadarColor(new Color(200, 200, 70));
		setScanColor(Color.white);
		setBulletColor(Color.blue);
		// Mira independente do robo
		setAdjustGunForRobotTurn(true);
		// Gira infinitamente o radar
		turnRadarRightRadians(Double.POSITIVE_INFINITY);
	}

	public void onScannedRobot(ScannedRobotEvent e) {
	    // Direção absoluta para o alvo
		double absBearing = e.getBearingRadians() + getHeadingRadians();
		// Velocidade lateral do alvo perpendicular a mim
		double latVel = e.getVelocity() * Math.sin(e.getHeadingRadians() - absBearing);
		double gunTurnAmt;
		// Mantem radar no alvo
		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
		if (Math.random() > .9) {
		    // Variar a velocidade aleatoriamente para dificultar previsão de oponentes
			setMaxVelocity((12 * Math.random()) + 12);
		}
		if (e.getDistance() > 300) {
		    // Aproximação mantendo mira fixa no alvo
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 22);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnRightRadians(
					robocode.util.Utils.normalRelativeAngle(absBearing - getHeadingRadians() + latVel / getVelocity()));
			setAhead((e.getDistance() - 140) * moveDirection);
			setFire(3);
		} else {
		    // Circular alvo perpendicular para maior velocidade de escape
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 15);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnLeft(-90 - e.getBearing());
			setAhead((e.getDistance() - 140) * moveDirection);
			setFire(3);
		}
	}

	public void onHitWall(HitWallEvent e) {
	    // Inverter direção caso acerte parede
		moveDirection = -moveDirection;
	}

	public void onWin(WinEvent e) {
	    // Celebrar
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
}
